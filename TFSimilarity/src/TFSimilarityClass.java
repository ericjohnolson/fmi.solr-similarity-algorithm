// Note that for this example and how to deploy I followed this blog:
// http://solrjack.blogspot.com/2013/07/how-to-compile-custom-similarity-class.html


import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.similarities.DefaultSimilarity;

public class TFSimilarityClass extends DefaultSimilarity {
	@Override
    public float idf(long docFreq, long numDocs) {
        return (float) 1.0;
    }
}

